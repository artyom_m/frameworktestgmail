package helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DriverProvider {

    private static WebDriver driver;

    public static final String CORRECT_ADDRESS = "thecat.pw@gmail.com";
    public static final String CORRECT_PASSWORD = "Gradient2thecat";
    public static final String PAGE_URL = "http://gmail.com/";


   // static {System.setProperty("webdriver.gecko.driver", "C:\\Users\\root\\Downloads\\geckodriver.exe");};
   static {System.setProperty("webdriver.gecko.driver", "C:\\Users\\root\\IdeaProjects\\TestGmailFramework\\geckodriver.exe");};
   static {System.setProperty("webdriver.chrome.driver", "C:\\Users\\root\\IdeaProjects\\TestGmailFramework\\chromedriver.exe");};

    private static Logger log = LoggerFactory.getLogger(DriverProvider.class);

    private  DriverProvider () {};

   /* public static WebDriver getDriver() {
        log.info("Инициализируем драйвер");
        if (driver == null) {
            driver = new FirefoxDriver();
            log.info("Драйвер проинициализированн");
        }
        return driver;
    }*/

   public static WebDriver getDriver(String browser){
       if (driver == null) {
           log.debug("driver==null");
           if(browser.equals("firefox")){
               log.info("Инициализируем драйвер {} ", browser);
               driver = new FirefoxDriver();
           }
           if(browser.equals("chrome")){
               log.info("Инициализируем драйвер {}", browser);
               driver = new ChromeDriver();
           }
           log.info("Драйвер проинициализированн");
       }

       return driver;
   }
    public static void exitDriver(){
        driver.quit();
        driver = null;
        log.info("Драйвер закрыт");
    }
}
