package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WriteMessagePage extends Page {

    public WriteMessagePage(String drivername) {
        super(drivername);
    }

    Logger log = LoggerFactory.getLogger(this.getClass());

    //кнопка написать
    @FindBy(css="div.T-I.J-J5-Ji.T-I-KE.L3")
    public WebElement writeBtn;

    //поле ввода кому
    @FindBy(css="textarea.vO[name=to]")
    public WebElement labelTo;

    //поле ввода тема
    @FindBy(css="input.aoT[name=subjectbox]")
    public WebElement labelSabject;

    //поле ввода текста
    @FindBy(css = "div.Am.Al.editable.LW-avf")
    public WebElement labelText;

    //крестик закрыть и сохранить
   @FindBy(css ="img.Ha")
    public WebElement saveCloseBtn;

   // @FindBy(css="a.J-Ke.n0")
    @FindBy(css="a[title^='Черновики']")
    public WebElement draftsLinkBtn;

   // @FindBy(css="T-ays-W095bf")
   // public WebElement saveCloseBtn;


    //жмем кнопку написать
    public void submitWriteBtn(){
        log.debug("Жмем НАПИСАТЬ");
        writeBtn.click();
    }

    public WriteMessagePage writeTo(String address){
        log.debug("адресат");
        labelTo.sendKeys(address);
        return this;
    }

    public WriteMessagePage writeSubject(String subject) {
        log.debug("write subject {}", subject);
        labelSabject.sendKeys(subject);
        return this;
    }

    public WriteMessagePage writeText(String text){
        log.debug("write text {}", text);
        labelText.sendKeys(text);
        return this;
    }

    public void submitSaveCloseBtn(){
        log.debug("Click Save and Close");
        saveCloseBtn.click();
    }

    public void submitDraftsLink(){
        draftsLinkBtn.click();
    }
}
