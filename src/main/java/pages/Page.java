package pages;

import org.openqa.selenium.support.PageFactory;

import static helpers.DriverProvider.getDriver;

public abstract class Page {
    Page(String drivername) {
        PageFactory.initElements(getDriver(drivername), this);
    }
}
