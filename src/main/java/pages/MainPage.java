package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static helpers.DriverProvider.CORRECT_ADDRESS;

public class MainPage extends Page {

private final Logger log = LoggerFactory.getLogger(this.getClass());

    @FindBy(css="div.T-I.J-J5-Ji.T-I-KE.L3")
    public WebElement labelWrite;

    public MainPage(String drivername){
        super(drivername);
    }


    public boolean verifyUser(String text){
        log.debug("Проверяем на корректный вход на почту по кнопке: '{}'", text);
        return text.equals(labelWrite.getText());
    }
}

