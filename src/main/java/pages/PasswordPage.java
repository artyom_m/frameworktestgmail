package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PasswordPage extends Page {

    public PasswordPage(String drivername){
        super(drivername);
    }

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @FindBy(css = "div#profileIdentifier")
    public WebElement verifyAddressLabel;

    @FindBy(css = "input[name=\"password\"]")
    public WebElement passwordField;


    @FindBy(css = "div#passwordNext")
    private WebElement passwordNextBtn;


    public boolean verifyAddress(String address){
        return address.equals(verifyAddressLabel.getText());
    }

    public PasswordPage fillPassword(String password){
        log.debug("Заполняем поле пароля значением: '{}'", password);
        passwordField.sendKeys(password);
        return this;
    }

    public void submitPasswordBtn(){
        log.debug("Нажимаем кнопку Далее");
        passwordNextBtn.click();
        log.info("Переходим на главную");
    }
}
