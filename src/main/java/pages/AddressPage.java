package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AddressPage extends Page {

    Logger log = LoggerFactory.getLogger(this.getClass());

    @FindBy(css = "input#identifierId")
    private WebElement addressField;

    @FindBy(css = "div#identifierNext")
    private WebElement addressNextBtn;


    public AddressPage(String drivername){
        super(drivername);
    }

    public AddressPage fillAddress(String address) {
        log.debug("Заполняем поле адреса значением: '{}'", address);
        addressField.sendKeys(address);
        return this;
    }

    public void submitAddressBtn(){
        log.debug("Жмем Далее");
        addressNextBtn.click();
        log.info("Переходим на страницу ввода пароля");
    }

}
