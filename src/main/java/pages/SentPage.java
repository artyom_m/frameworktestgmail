package pages;

import helpers.DriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SentPage extends Page {

   public SentPage(String drivername) {
        super(drivername);
    }

    Logger log = LoggerFactory.getLogger(this.getClass());

   //поиск письма в списке отправленных по теме
    @FindBy(xpath="//span[text()='subject3']")
    public WebElement messageInSent;

    //Ссылка Отправленные
    @FindBy(css="a[title = 'Отправленные']")
    public WebElement linkSent;

    //круглая кнопка справа сверху
    @FindBy(css="a.gb_b.gb_fb.gb_R")
    public WebElement accountCircle;

    //кнопка выйти из аккаунта
    @FindBy(css="a#gb_71.gb_Ea.gb_Pf.gb_Wf.gb_De.gb_Db")
    public WebElement exitBtn;

 @FindBy(css = "input[name=\"password\"]")
 public WebElement checkPassField;

    //проверка на выход из аккаунта
    public boolean verifyAddressField(String drivername){
        return DriverProvider.getDriver(drivername).findElement(By.cssSelector("input[name=\"password\"]")).isDisplayed();
    }

    //есть ли письмо в отправленных
    public boolean verifyMsgInSent(String drivername){
      return  DriverProvider.getDriver(drivername).findElement(By.xpath("//span[text()='subject3']")).isDisplayed();
    }

}
