package stepdefinitios;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.Scenario;
import helpers.DriverProvider;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import pages.AddressPage;
import pages.MainPage;
import pages.PasswordPage;

import java.util.concurrent.TimeUnit;

import static helpers.DriverProvider.PAGE_URL;
import static helpers.DriverProvider.getDriver;

public class AutorizationSteps {
    private AddressPage addressPage;
    private PasswordPage passwordPage;
    private WebDriverWait wait;
    private final String BRO = "firefox";
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Before
    public void setUp() {
        getDriver(BRO).manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        wait = (WebDriverWait) new WebDriverWait(getDriver(BRO), 60).withMessage("----Element not found-----");
        log.debug("Предустановки");
    }

    @Given("^user navigate to <(.+)>$")
    public void openPage(String url){
        getDriver(BRO).navigate().to(url);
        log.info("user navigate to {}", url);
       //AddressPage addressPage = new AddressPage();
        //PasswordPage passwordPage = new PasswordPage();
    }
//------------------Address page-------------------------
    @And("^user enter address <(.+)>$")
    public void fillAddress(String text){
        AddressPage addressPage = new AddressPage(BRO);
        log.info("user enter address {}", text);
        addressPage.fillAddress(text);
    }

    @And("^click submit button on address page$")
    public void submitAddressBtn(){
        AddressPage addressPage = new AddressPage(BRO);
        log.info("click submit button on address page");
        addressPage.submitAddressBtn();
    }

//-------------------Password page----------------------------
    @And("^user enter password <(.+)>$")
    public void fillPassword(String text){
        PasswordPage passwordPage = new PasswordPage(BRO);
        wait.until(ExpectedConditions.visibilityOf(passwordPage.verifyAddressLabel));
        log.info("user enter password {}", text);
        passwordPage.fillPassword(text);
    }


    @And("^click submit button on password page$")
    public void submitPasswordBtn(){
        PasswordPage passwordPage = new PasswordPage(BRO);
        log.info("click submit button on password page");
        passwordPage.submitPasswordBtn();
    }

    @Then("he should see <(.+)> write message")
    public void verifyAutorizationGmail(String text){
        wait.until(ExpectedConditions.visibilityOf(new MainPage(BRO).labelWrite));
        Assert.assertTrue(new MainPage(BRO).verifyUser(text), "++Unverify++");
        log.info("--------------Конец теста----------------");
    }

    @After
    public void tearDown(){
        log.debug("Постустановки");
        log.debug("postustanovki");
        DriverProvider.exitDriver();
    }
}
