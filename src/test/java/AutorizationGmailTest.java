import com.beust.jcommander.Parameter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;
import pages.*;
import helpers.DriverProvider;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

import static helpers.DriverProvider.CORRECT_ADDRESS;
import static helpers.DriverProvider.CORRECT_PASSWORD;
import static helpers.DriverProvider.PAGE_URL;
import static helpers.DriverProvider.getDriver;


public class AutorizationGmailTest {


private WebDriverWait wait;

private final Logger log = LoggerFactory.getLogger(this.getClass());


@BeforeTest
@Parameters("drivername")
    public void setUp(String drivername){
    getDriver(drivername).manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    wait = (WebDriverWait) new WebDriverWait(getDriver(drivername), 30).withMessage("----Element not found-----");
    log.debug("Запускаем страницу: '{}'", PAGE_URL);
    getDriver(drivername).navigate().to(PAGE_URL);


}
@AfterTest
    public void tearDown(){
    DriverProvider.exitDriver();
}

@Parameters("drivername")
@Test//Проверяем корректность введенного адреса
    public void addressTest(String drivername){
    log.debug("Тест на корректный адресс");
    AddressPage addressPage = new AddressPage(drivername);
    addressPage.fillAddress(CORRECT_ADDRESS);
    addressPage.submitAddressBtn();
    wait.until(ExpectedConditions.visibilityOf(new PasswordPage(drivername).verifyAddressLabel));
    Assert.assertTrue(new PasswordPage(drivername).verifyAddress(CORRECT_ADDRESS), "Unverify Addres");//Проверка по полю ввода пароля
}

@Parameters("drivername")
@Test(dependsOnMethods = {"addressTest"})//Проверяем корректность пароля, тест зависит от корректности адреса
    public void passwordTest(String drivername){
    log.debug("Тест на корректный пароль");
    PasswordPage passwordPage = new PasswordPage(drivername);
    passwordPage.fillPassword(CORRECT_PASSWORD);
    passwordPage.submitPasswordBtn();
    wait.until(ExpectedConditions.visibilityOf(new MainPage(drivername).labelWrite));
    Assert.assertTrue(new MainPage(drivername).verifyUser("НАПИСАТЬ"), "++Unverify++");//Проверка по кнопке НАПИСАТЬ
}

@Parameters("drivername")
@Test(dependsOnMethods = {"passwordTest"})//тест на создание письма
public void writeMessage(String drivername) {
    WriteMessagePage wmPage = new WriteMessagePage(drivername);
    wmPage.submitWriteBtn();
    wait.until(ExpectedConditions.visibilityOf(wmPage.labelTo));
    wmPage.writeTo("doomgl2@mail.ru");
    wmPage.writeSubject("subject3");
    wmPage.writeText("text");
    wmPage.submitSaveCloseBtn();
    wait.until(ExpectedConditions.visibilityOf(wmPage.draftsLinkBtn));
    wmPage.submitDraftsLink();
    //DriverProvider.getDriver(drivername).navigate().
    wait.until(ExpectedConditions.visibilityOf(new DraftPage(drivername).verifyVisibleDraft)).isDisplayed();
    Assert.assertTrue(new DraftPage(drivername).verifyDraft(drivername), "Письмо похоже не создалось");
}

@Parameters("drivername")
@Test(dependsOnMethods = {"writeMessage"})//тест на коректное сохранение письма в черновиках
public void checkDraft(String drivername){
    DraftPage draftPage = new DraftPage(drivername);
   // draftPage.draftsLinkBtn.click();
    Assert.assertTrue(draftPage.verifyDraft(drivername), "Не найден subject");

}

@Parameters("drivername")
@Test(dependsOnMethods = {"checkDraft"})//тест на открытие черновика
public void openDraft(String drivername){
    DraftPage draftPage = new DraftPage(drivername);
    draftPage.draftLink.click();
    Assert.assertTrue(draftPage.verifyOpenDraft(drivername), "Черновик не открывается");
}

@Parameters("drivername")
@Test(dependsOnMethods = {"openDraft"})//тест на проверку сохранения в письме из черновика полей адресат, тема, тело письма
    public void checkToSubText(String drivername){
    DraftPage draftPage = new DraftPage(drivername);
    Assert.assertTrue(draftPage.verifyToSubjectText("doomgl2@mail.ru", "subject3", "text"));
}

@Parameters("drivername")
@Test(dependsOnMethods = {"checkToSubText"})//тест отправки черновика
    public void checkSendDraft(String drivername){
    DraftPage draftPage = new DraftPage(drivername);
    draftPage.draftSendBtn.click();
    wait.until(ExpectedConditions.visibilityOf(draftPage.labelSendMessage));
    Assert.assertTrue(draftPage.veryfyOtpravleno(drivername), "Письмо не отправлено.");
}

@Parameters("drivername")
@Test(dependsOnMethods = {"checkSendDraft"})//тест - письмо удаляется из черновиков после отправки
    public void checkDelFromDrafts(String drivername){
    DraftPage draftPage = new DraftPage(drivername);
    Assert.assertFalse(draftPage.verifyDraft(drivername), "Писцмо нэ удалилос");
}

@Parameters("drivername")
@Test(dependsOnMethods = {"checkDelFromDrafts"})//тест - письмо добавляется в список отправленных после отправки
    public void checkMessageInSent(String drivername) {
    SentPage sentPage = new SentPage(drivername);
    sentPage.linkSent.click();
    wait.until(ExpectedConditions.visibilityOf(sentPage.messageInSent));
    Assert.assertTrue(sentPage.verifyMsgInSent(drivername), "Сообщение не найденно в отправленных");
}

@Parameters("drivername")
@Test(dependsOnMethods = {"checkMessageInSent"})//тест-выход из учетной записи
    public void checkExit(String drivername){
    SentPage sentPage = new SentPage(drivername);
    sentPage.accountCircle.click();
    sentPage.exitBtn.click();
    wait.until(ExpectedConditions.visibilityOf(sentPage.checkPassField));
    Assert.assertTrue(sentPage.verifyAddressField(drivername), "Выход из аккаунта не произошел");
}

}
