import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        features="src/test/resources",
        glue="stepdefinitios")

public class GRunner extends AbstractTestNGCucumberTests {

}
